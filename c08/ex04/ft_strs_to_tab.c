/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strs_to_tab.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thlefran <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/14 22:30:11 by thlefran          #+#    #+#             */
/*   Updated: 2019/09/15 03:52:24 by thlefran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_stock_str.h"
#include <stdlib.h>

int						ft_strlen(char *str)
{
	int		i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

char					*ft_strcpy(char *dest, char *src)
{
	int			i;

	i = 0;
	while (src[i] != '\0')
	{
		dest[i] = src[i];
		i++;
	}
	dest[i] = '\0';
	return (dest);
}

char					*ft_strdup(char *src)
{
	char *tab;

	tab = (char*)malloc(sizeof(*src) * (ft_strlen(src) + 1));
	if (tab != NULL)
	{
		ft_strcpy(tab, src);
		return (tab);
	}
	return (NULL);
}

struct	s_stock_str		*ft_strs_to_tab(int ac, char **av)
{
	int				i;
	t_stock_str		*dest;

	i = 0;
	if (!ac)
	{
		dest = malloc(sizeof(char) * 1);
		dest[0].str = 0;
		return (dest);
	}
	if (!(dest = (t_stock_str*)malloc(sizeof(t_stock_str) * (ac + 1))))
		return (NULL);
	while (i < ac)
	{
		dest[i].str = av[i];
		dest[i].size = ft_strlen(dest[i].str);
		dest[i].copy = ft_strdup(av[i]);
		i++;
	}
	dest[i].str = 0;
	return (dest);
}
