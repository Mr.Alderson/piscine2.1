/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_convert_base.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thlefran <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/11 21:56:47 by thlefran          #+#    #+#             */
/*   Updated: 2019/09/17 03:11:17 by thlefran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
void	ft_putchar(char c)
{
	write(1, &c, 1);
}
int ft_checkbase(char *base)
{
	int i;
	int j;
	int len;

	len = 0;
	i = 0;
	j = i + 1;
	while (base[len])
		len++;
	if (len >= 2)
	{
		while (base[i])
		{
			while (base[j] != '\0')
			{
				if (base[i] == base[j] || base[i] == '+' || base[i] == '-')
					return (0);
				j++;
			}
			i++;
			j = i + 1;
		}
		return (len);
	}
	return (0);
}

char	ft_itoa_base(int nbr, char *base)
{
	unsigned int i;
	unsigned int div_base;
	unsigned int nb;
	char *result;
	i = 1;
	div_base = ft_checkbase(base);
	if (div_base == 0)
		return (0);
	if (nbr < 0)
	{
		nb = -nbr;
		ft_putchar('-');
	}
	else
		nb = nbr;
	while (nb / i >= div_base)
		i = i * div_base;
	while (i > 0)
	{
		result = malloc(sizeof(char *) * (base[(nb / i % div_base)]));
		i = i / div_base;
	}
	return (*result);
}
int		ft_atoi_base(char *str, char *base)
{
	int		i;
	int		n;
	int		negatif;
	int		div_base;

	negatif = 1;
	n = 0;
	i = 0;
	div_base = ft_checkbase(base);
	if (div_base == 0 || div_base == 1)
		return (0);
	while (str[i] == '\f' || str[i] == '\t' || str[i] == '\n'
			|| str[i] == '\r' || str[i] == '\v' || str[i] == ' ')
		i++;
	while (str[i] == '-' || str[i] == '+')
	{
		if (str[i] == '-')
			negatif = negatif * -1;
		i++;
	}
	while (str[i] >= '0' && str[i] <= '9')
	{
		n = n * div_base + str[i] - 48;
		i++;
	}
	return (negatif * n);
}

char	*ft_convert_base(char *nbr, char *base_from, char *base_to)
{
	int		nb_decimal;
	char	*result;

	nb_decimal = ft_atoi_base(nbr, base_from);
	*result = ft_itoa_base(nb_decimal, base_to);
	return (result);
}

int main(void)
{
	char nbr[] = "420";
	char base_from[] = "0123456789";
	char base_to[] = "01";
	printf("Result --> %s\n", ft_convert_base(nbr, base_from, base_to));
	return (0);
}
