/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_utlimate_range.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thlefran <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/11 21:56:02 by thlefran          #+#    #+#             */
/*   Updated: 2019/09/15 22:13:05 by thlefran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>
int		*ft_utlimate_range(int **range, int min, int max)
{
	int		i;

	i = 0;
	*range = malloc((max - min) * (sizeof(int)));
	if (*range == NULL)
		return (0);
	if (min >= max)
		(*range) = 0;
	while (min < max)
	{
		(*range[i]) = min;
		min++;
		i++;
	}
	return (*range);
}

int main(void)
{
	int i;
	int min = 5;
	int max = 10;
	int **range;
	int tab[] = {1, 2, 3, 4};
	i = 0;
	range  = ft_utlimate_range(*range, min, max);
	while (i < (max - min))
	{
		printf("result --> %d\n", (range[i]));
		i++;
	}
	return (0);
}
