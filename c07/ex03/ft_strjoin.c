/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thlefran <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/11 21:56:18 by thlefran          #+#    #+#             */
/*   Updated: 2019/09/18 01:58:18 by thlefran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		ft_strlen(char *str)
{
	int		i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

char	*ft_strcat(char *dest, char *src)
{
	int		i;
	int		j;

	j = 0;
	i = ft_strlen(dest);
	if (!dest || !src)
		return (0);
	while (src[j])
	{
		dest[i] = src[j];
		i++;
		j++;
	}
	dest[i] = '\0';
	return (dest);
}

int		ft_fullsize(char **strs, char *sep)
{
	int		i;
	int		j;
	int		f_size;

	f_size = 0;
	i = 0;
	j = 0;
	while (strs[i])
	{
		f_size = f_size + ft_strlen(strs[i]);
		i++;
	}
	f_size += ft_strlen(sep) * (i - 1);
	return (f_size);
}

char	*ft_strjoin(int size, char **strs, char *sep)
{
	char	*tab;
	int		i;
	int		j;
	int		fullsize;

	fullsize = ft_fullsize(strs, sep);
	tab = malloc(sizeof(char) * (ft_fullsize(strs, sep) + 1));
	j = 0;
	i = 0;
	if (size == 0)
		return (NULL);
	while (strs[i])
	{
		tab = ft_strcat(tab, strs[i]);
		if (!(strs[i + 1] == '\0'))
			tab = ft_strcat(tab, sep);
		i++;
	}
	tab[fullsize] = '\0';
	return (tab);
}
