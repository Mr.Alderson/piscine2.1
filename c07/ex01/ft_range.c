/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_range.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thlefran <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/11 21:55:32 by thlefran          #+#    #+#             */
/*   Updated: 2019/09/15 21:44:11 by thlefran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>

int		*ft_range(int min, int max)
{
	int		*tab;
	int		i;

	i = 0;
	tab = malloc((max - min) * (sizeof(int)));
	if (tab == NULL)
		return (0);
	if (min >= max)
		*tab = 0;
	while (min < max)
	{
		tab[i] = min;
		min++;
		i++;
	}
	return (tab);
}

int main(void)
{
	int i;
	int min = 5;
	int max = 10;
	int *tab;

	i = 0;
	tab = ft_range(min, max);
	while (i < (max - min))
	{
		printf("result --> %d\n", tab[i]);
		i++;
	}
	return (0);
}
