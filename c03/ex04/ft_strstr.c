/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thlefran <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/02 19:14:02 by thlefran          #+#    #+#             */
/*   Updated: 2019/09/09 00:42:25 by thlefran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strstr(char *str, char *to_find)
{
	int		i;
	int		j;
	int		aiguille;

	i = 0;
	j = 0;
	aiguille = 0;
	while (to_find[aiguille] != '\0')
		aiguille++;
	if (aiguille == 0)
		return (str);
	while (str[i] != '\0')
	{
		while (to_find[j] == str[i + j])
		{
			if (j == aiguille - 1)
				return (str + i);
			j++;
		}
		j = 0;
		i++;
	}
	return (0);
}
