/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thlefran <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/04 17:13:57 by thlefran          #+#    #+#             */
/*   Updated: 2019/09/05 00:51:50 by thlefran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <string.h>

char	*ft_strcpy(char *dest, char *src);
char	*ft_strncpy(char *dest, char *src, unsigned int n);
int		ft_str_is_alpha(char *str);
int		ft_str_is_numeric(char *str);
int		ft_str_is_lowercase(char *str);
int		ft_str_is_uppercase(char *str);
int		ft_str_is_printable(char *str);
char	*ft_strupcase(char *str);
char	*ft_strlowcase(char *str);
char	*ft_strcapitalize(char *str);

int		main(void)
{
	int		i;

	i = 4;
	if (i == 0)
	{
		char src[] = "salut";
		char dest[] = "kfjbgk";
		ft_strcpy(dest, src);
		printf("ma fonction -->%s\n", ft_strcpy(dest, src));
		printf("la vraie -->%s\n", strcpy(dest, src));
		return(0);
	}
	else if (i == 1)
	{
		char src[] = "salut";
		char dest[] = "jfjfjff";
		ft_strncpy(dest, src, 0);
		printf("%s\n", dest);
		return (0);
	}
	else if (i == 2)
	{
		char src[] = "couc6ou";
		ft_str_is_alpha(src);
		printf("%d", ft_str_is_alpha(src));
		return(0);
	}
	else if (i == 3)
	{
		char str[] = "jdfbsk";
		ft_str_is_numeric(str);
		printf("%d",ft_str_is_numeric(str));
		return (0);
	}
	else if (i == 4)
	{
		char str[] = "coUcou";
		ft_str_is_lowercase(str);
		printf("%d", ft_str_is_lowercase(str	));
		return(0);
	}
	else if (i == 5)
	{
		char str[] = "vq#qq$%qq^TUdgrefrgG";
		ft_str_is_uppercase(str);
		printf("%d", ft_str_is_uppercase(str));
		return(0);
	}
	else if (i == 6)
	{
		char str[] = "\n truc";
		ft_str_is_printable(str);
		printf("%d", ft_str_is_printable(str));
		return(0);
	}
	else if (i == 7)
	{
		char str[] = "HGjhdbfg";
		ft_strupcase(str);
		printf("%s", ft_strupcase(str));
		return(0);
	}
	else if (i == 8)
	{
		char src[] = "UHGjhghHGIH";
		ft_strlowcase(src);
		printf("%s", src);
		return(0);
	}
	else if (i == 9)
	{
		char str[] = "            salut, comnt tu vAs ? 42mOts quarante-deux; cinQuante+et+un";
		printf("%s",ft_strcapitalize(str));
		return(0);
	}
}
