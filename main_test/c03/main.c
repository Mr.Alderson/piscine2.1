/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thlefran <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/04 17:42:56 by thlefran          #+#    #+#             */
/*   Updated: 2019/09/05 00:52:34 by thlefran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <stdio.h>

int		ft_strcmp(char *s1, char *s2);
int		ft_strncmp(char *s1, char *s2, unsigned int n);
char	*ft_strcat(char *dest, char *src);
char	*ft_strncat(char *s, char *d, unsigned int n);
char	*ft_strstr(char *s, char *d);

int		main(void)
{
	int					i = 0;

	if (i == 0)
	{
		char s1[] = "salut";
		char s2[] = "truc";

		printf("la vraie -->%d\n", strcmp(s1, s2));
		printf("la mienne -->%d\n", ft_strcmp(s1, s2));
	}
	else if (i == 1)
	{
		char s1[] = "salut";
		char s2[] = "truc";
		unsigned int n = 3;
		printf("la vraie -->%d\n", strncmp(s1, s2, n));
		printf("la mienne ->%d\n", ft_strncmp(s1, s2,n));
	}
	else if (i == 2)
	{
		char dest[] = "rrr";
		char src[] = "";
		printf("la vraie --> %s\n", strcat(dest, src));
		printf("la mienne --> %s\n", ft_strcat(dest, src));
	}
	else if (i == 3)
	{
		unsigned int	n = 3;
		char	dest[] = "rrr";
		char	src[] = "";
		printf("la vraie  --> %s\n", strncat(dest, src, n));
		printf("la mienne --> %s\n", ft_strncat("salut", "-----", n));
	}
	else if (i == 4)
	{
		printf("la vraie  --> %s\n",    strstr("abcdedefh", "defh"));
		printf("la mienne --> %s\n", ft_strstr("abcdedefh", "defh"));
	}
	return (0);
}
