/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_param.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thlefran <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/07 21:50:48 by thlefran          #+#    #+#             */
/*   Updated: 2019/09/07 22:03:48 by thlefran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		check_param(char *str)
{
	int		i;
	int		param;

	i = 0;
	param = 0;

	while (str[i])
	{
		if ((i % 2) == 0 && (str[i] >= '0' && str[i] <= '9'))
		{
			param = param * 10 + str[i] - 48;
			i++;
		}
		else if ((i % 2) == 1 && (str[i] == ' '))
			i++;
	}
	return (param);
}
