/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ateixeir <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/08 20:49:51 by ateixeir          #+#    #+#             */
/*   Updated: 2019/09/08 22:33:18 by ateixeir         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "prot.h"

int		check_x(int v, int grille[4][4], int x)
{
	int y;

	y = 0;
	while (y < 4)
	{
		if (grille[x][y] == v)
			return (0);
		y++;
	}
	return (1);
}

int		check_y(int v, int grille[4][4], int y)
{
	int x;

	x = 0;
	while (x < 4)
	{
		if (grille[x][y] == v)
			return (0);
		x++;
	}
	return (1);
}

int		check_indice_x(int grille[4][4], int *indice, int x, int y)
{
	int z;

	z = 0;
	if (y == 3)
	{
		if (check_up(grille, indice, y))
			z++;
		if (check_right(grille, indice, x))
			z++;
	}
	else
		z = 2;
	if (z == 2)
		return (1);
	return (0);
}

int		check_indice_y(int grille[4][4], int *indice, int y, int x)
{
	int z;

	z = 0;
	if (x == 3)
	{
		if (check_up(grille, indice, y))
			z++;
		if (check_down(grille, indice, y))
			z++;
	}
	else
		z = 2;
	if (z == 2)
		return (1);
	return (0);
}
